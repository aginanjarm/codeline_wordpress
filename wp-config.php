<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'codeline_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8dR{]M6rzb9;3)A|moQZH]K^T#Nw*w (71ot:J8&q2m^Ei-H_o*+=C*3&_UN t-P');
define('SECURE_AUTH_KEY',  '*qK/hx~+<R|dExvbr9{Hz(SLbY&uQF8-@)=*!TY*S;NLvL;U9A 9SFKA$3D!c,99');
define('LOGGED_IN_KEY',    '76I4.u4ndT~zi:+sr~-h=/L}J%W.zGxkBu>Yd]:OOZ1pJgKZ+;snyb4}+;C7PR0O');
define('NONCE_KEY',        ')IP]Z;I|kn^d^f])6_JTguVw*IBNFj(ab#b~,O^R&^!l1w~17:&w$n`;~.j4cGi!');
define('AUTH_SALT',        '<;gjLrq6TSeN0V2vOuV |>[Hw!4Uc.?qm`xm38LPl%Gl?*{Z&*cyS=~<.i$!8$6B');
define('SECURE_AUTH_SALT', 'Ed:Yt_~c)ZsFjW;adLbi,Aw(*>Ys(]JJ<0 o1pQ@Z{}/p fRG&am^>d87oCzlAhk');
define('LOGGED_IN_SALT',   'MZE<2_j_!2D`_lB9x##/xHT2_ YUhg1=0Hg=vGpk.)%z2vheaiX1GlSnHiwB7<4b');
define('NONCE_SALT',       'prOf=ZZ`kaOQG&<)~9S/39[Yw;ZzYrolQ1[E>9>IBS&)5*)yO}SG ,~lLnToG3DH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
