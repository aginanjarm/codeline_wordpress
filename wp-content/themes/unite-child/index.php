<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package unite
 */

get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-8">
		<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) : ?>
			<?php $args = array(
				'post_type' => 'film',
				'posts_per_page'=>3,
				'tax_query' => array(
					    array(
					        'taxonomy' => 'genre_taxonomy',
							'operator' => 'OR'
					    ),
					    array(
					        'taxonomy' => 'actor',
							'operator' => 'OR'
					    )
					)
				); ?>
            <?php 
            	$loop = null;
            	$loop = new WP_Query($args); 
            ?>
			<?php /* Start the Loop */ ?>
			<?php 
			while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<article id="post-<?php the_ID();?>" class="post-<?php the_ID();?>">
					<header class="entry-header page-header">
						<a href="<?php the_permalink() ?>" title="<?php the_title();?>"></a>
						<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					</header><!-- .entry-header -->

					<div class="entry-content">
							<p><?php the_content(); ?></p>
							<p>
								<?php
									$year = get_the_term_list( $post->ID, 'country', 'Country : ', ', ', '' );
									if($year)
										echo $year . '<br />';
									$genre = get_the_term_list( $post->ID, 'genre_taxonomy', 'Genres : ', ', ', '' );
									if($genre)
										echo $genre . '<br />';
									$ticket_price = get_post_meta($post->ID,'ticket_price',true);
									if($ticket_price)
										echo 'Ticket Price: ' . $ticket_price . '<br />';
									$release_date = get_post_meta($post->ID,'release_date',true);
									if($release_date)
										echo 'Release Date: ' . $release_date . '<br />';
								?>
							</p>
					</div><!-- .entry-content -->
					
					<hr class="section-divider">
				</article>

			<?php endwhile; ?>

			<?php unite_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
