<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package unite
 */
?>
	<div id="secondary" class="widget-area col-sm-12 col-md-4" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

			<aside id="search" class="widget widget_search">
				<?php get_search_form(); ?>
			</aside>

			<aside id="archives" class="widget">
				<h1 class="widget-title"><?php _e( 'Archives', 'unite' ); ?></h1>
				<ul>
					<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
				</ul>
			</aside>

			<aside id="meta" class="widget">
				<h1 class="widget-title"><?php _e( 'Meta', 'unite' ); ?></h1>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</aside>

		<?php endif; // end sidebar widget area ?>
		<?php if ( have_posts() ) : ?>
			<?php $args = array(
				'post_type' => 'film',
				'posts_per_page'=>5
				); ?>
            <?php 
            	$loop = null;
            	$loop = new WP_Query($args); 
            ?>
			<?php /* Start the Loop */ ?>
			<aside id="recent-posts-2" class="widget widget_recent_entries">		
					<h3 class="widget-title">Last Films</h3>		
						<ul>
			<?php 
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<li>
								<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
							</li>
			<?php endwhile; ?>
						</ul>
				</aside>
		<?php endif; ?>
	</div><!-- #secondary -->
