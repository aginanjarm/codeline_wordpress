<?php
/**
 * The Template for displaying all single posts.
 *
 * @package unite
 */

get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID();?>" class="post-<?php the_ID();?>">
				<header class="entry-header page-header">
					<a href="<?php the_permalink() ?>" title="<?php the_title();?>"></a>
					<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				</header><!-- .entry-header -->
				<div class="entry-content">
						<p><?php the_content(); ?></p>
						<p>
							<?php
								$year = get_the_term_list( $post->ID, 'country', 'Country : ', ', ', '' );
								echo $year . '<br />';
								$genre = get_the_term_list( $post->ID, 'genre_taxonomy', 'Genres : ', ', ', '' );
								echo $genre . '<br />';
								$ticket_price = get_post_meta($post->ID,'ticket_price',true);
								echo 'Ticket Price: ' . $ticket_price . '<br />';
								$release_date = get_post_meta($post->ID,'release_date',true);
								echo 'Release Date: ' . $release_date . '<br />';
								// var_dump(
								// 	get_post_meta(44)
								// );
							?>
						</p>
				</div><!-- .entry-content -->
				
				
				<hr class="section-divider">
			</article>
		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>